var Server = IgeClass.extend({
	classId: 'Server',
	Server: true,

	init: function (options) {
		var self = this;
		ige.timeScale(1);            

		// Add the server-side game methods / event handlers
		this.implement(ServerNetworkEvents);
        
        self.playerManager = new PlayerManager();
        self.movementManager = new MovementManager();
        self.sceneManager = new SceneManager();

		// Add the networking component
		ige.addComponent(IgeNetIoComponent)
			// Start the network server
			.network.start(ServerNetworkEvents.port, function () {
				// Start the game engine
				ige.start(function (success) {
					// Check if the engine started successfully
					if (success) {
                        
                        ige.network.define('playerJoin', self._onPlayerJoin);
                        ige.network.define('message', self._onMessage);
						ige.network.define('playerPointToTile', self._onPlayerPointToTile);

						ige.network.on('connect', self._onConnect);
						ige.network.on('disconnect', self._onDisconnect);
                        
						// Add the network stream component
						ige.network.addComponent(IgeStreamComponent)
							.stream.sendInterval(100)
							.stream.start(); // Start the stream


						self.sceneManager.loadMaps(function() {
                            // Accept incoming connections
                            ige.network.acceptConnections(true);
						}, function(reason) {
							console.log(reason);
							ige.stop();
						});
					}
				});
			});
	}
});

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = Server; }