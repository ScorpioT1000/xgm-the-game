var config = {
	include: [
		{name: 'ServerNetworkEvents', path: './gameClasses/server/ServerNetworkEvents'},
		{name: 'PlayerManager', path: './gameClasses/server/PlayerManager'},
        {name: 'MovementManager', path: './gameClasses/server/MovementManager'},
		{name: 'JsonMapLoader', path: './gameClasses/JsonMapLoader'},
		{name: 'PlayerComponent', path: './gameClasses/PlayerComponent'},
		{name: 'SceneManager', path: './gameClasses/SceneManager'},

        {name: 'Unit', path: './gameClasses/world/Unit'},
        {name: 'Character', path: './gameClasses/world/Character'}
	]
};

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = config; }