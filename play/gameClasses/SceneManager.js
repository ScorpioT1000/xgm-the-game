var SceneManager = function() {
    var self = this;

    self.spawns = {};
    self.maps = [
        { name: 'Barrens', directory: 'barrens' }
    ];
    self.tileWidth = 32;
    self.tileHeight = 32;

    self.collisionMap = null;
    self.collisionMapFunction = null;
    self.pathFinder = null;

    // scenes
    self.mainScene = new IgeScene2d()
        .id('mainScene')
        .translateTo(0, 0, 0);


    self.backScene = new IgeScene2d()
        .id('backScene')
        .layer(0)
        .isometricMounts(true)
        .mount(self.mainScene);

    self.normalScene = new IgeScene2d()
        .id('normalScene')
        .layer(1)
        .mount(self.mainScene);

    self.frontScene = new IgeScene2d()
        .id('frontScene')
        .layer(2)
        .mount(self.mainScene);


    self.uiScene = new IgeScene2d()
        .id('uiScene')
        .layer(3)
        .ignoreCamera(true)
        .mount(self.mainScene);

    // layers

    self.objectLayer = new IgeTileMap2d()
        .id('objectLayer')
        .layer(0)
        .tileWidth(self.tileWidth)
        .tileHeight(self.tileHeight)
        .isometricMounts(true)
        .drawMouse(true)
        .mount(self.normalScene);

    self.unitLayer = new IgeTileMap2d()
        .id('unitLayer')
        .layer(1)
        .tileWidth(self.tileWidth)
        .tileHeight(self.tileHeight)
        .isometricMounts(true)
        .mount(self.normalScene);

    // Create the main viewport
    self.vp = new IgeViewport()
        .id('vp')
        .autoSize(true)
        .scene(self.mainScene)
        .mount(ige);

    self.getViewport = function() {
        return self.vp;
    };
    self.getCharacterScene = function() {
        return self.unitLayer;
    };
    self.getLandscapeScene = function() {
        return self.objectLayer;
    };
    self.getSpawnFor = function(who) {
        if(self.spawns[who]) {
            var s = self.spawns[who];
            return new IgePoint3d(s.x, s.y, 0);
        }
        return new IgePoint3d(0,0,0);
    };

    self.vp.translateToPoint(self.getSpawnFor('player'));


    self.loadMaps = function(onSuccess, onFail) {
        // Load the Tiled map data and handle the return data
        var map = self.maps[0]; // TODO: loop of maps for server, current map for client
        JsonMapLoader.load('maps/'+map.directory+'/map.json', function (layerArray, layersById) {
            var i;
            if(! layerArray.length) {
                onFail && onFail('Cannot load map ');
                return;
            } else {
                console.log('Loading map "'+map.name+'" ...');
            }
            self.objectLayer.gridSize(layersById.META.width, layersById.META.height);
            self.unitLayer.gridSize(layersById.META.width, layersById.META.height);

            if(ige.isServer) {
                self.collisionMap = layersById.PATH;
                self.collisionMapFunction = function(tileData, tileX, tileY) { return tileData !== 1; }
                self.pathFinder = new IgePathFinder().neighbourLimit(1000); // Set a high limit because we are using a large map
                if(layersById.META) {
                    self.processTiledMeta(layersById.META);
                }
                console.log('Map "'+map.name+'" loaded.');
            } else {
                for (i = 0; i < layerArray.length; i++) {
                    if(layerArray[i]._id == 'PATH') {
                        layerArray.splice(i, 1); --i;
                        continue;
                    } else if(layerArray[i].name && layerArray[i].name == 'META') {
                        self.processTiledMeta(layerArray[i]);
                        layerArray.splice(i, 1); --i;
                        continue;
                    }
                    // Before we mount the layer we will adjust the size of
                    // the layer's tiles because Tiled calculates tile width
                    // based on the line from the left-most point to the
                    // right-most point of a tile whereas IGE calculates the
                    // tile width as the length of one side of the tile square.
                    layerArray[i]
                        .tileWidth(layerArray[i].tileWidth() / 2)
                        .tileHeight(layerArray[i].tileHeight() / 2)
                        .autoSection(16)
                        .isometricMounts(true)
                        .layer(i)
                        .mount(layerArray[i]._id.charAt(0) == '_' ? self.frontScene : self.backScene);
                }
            }
            onSuccess && onSuccess();
            
        });
    };

    self.processTiledMeta = function(layer) {
        for(var i = 0; i < layer.objects.length; ++i) {
            var d = layer.objects[i].name.split(' ');
            if(d[0] == 'spawn' && d.length > 1) { // spawn player
                if(d.length > 2 && parseInt(d[2])) { // spawn zombie 5
                    layer.objects[i].count = parseInt(d[2]);
                } else {
                    layer.objects[i].count = 1;
                }
                self.spawns[d[1]] = layer.objects[i];
            }
        }

    };
};

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = SceneManager; }