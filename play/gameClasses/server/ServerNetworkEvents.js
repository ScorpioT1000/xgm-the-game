var ServerNetworkEvents = new (function() {
	var self = this;

    self.port = 8000;

    self._onConnect = function (socket) {
		return false; // Don't reject the client connection
	};

    self._onDisconnect = function (clientId) {
		ige.server.playerManager.leave(clientId);
	};

    self._onMessage = function(data, clientId) {
        // TODO: process message from client
    };

    self._onPlayerJoin = function (data, clientId) {
        ige.server.playerManager.join(data.identity, data.credentials, clientId);
	};

    self._onPlayerPointToTile = function (data, clientId) {
		var p = ige.server.playerManager.findByClientId(clientId);

        ige.server.movementManager.playerAddPath(p, parseInt(data[0]), parseInt(data[1]), 0);
	};
    return self;
});

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = ServerNetworkEvents; }