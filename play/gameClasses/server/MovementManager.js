var MovementManager = function() {
    var self = this;

    self.playerAddPath = function (player, x, y, z) {
        // Calculate the start tile from the current position by using the collision map
        // as a tile map (any map will do with the same tileWidth and height).
        var startTile = player._parent.pointToTile(player._translate.toIso());

        /*var newPath = ige.server.sceneManager.pathFinder.generate(ige.server.sceneManager.collisionMap,
         startTile,
         new IgePoint3d(parseInt(data[0]), parseInt(data[1]), 0),
         function (tileData, tileX, tileY) {
         // If the map tile data is set to 1, don't allow a path along it
         return tileData !== 1;
         }, true, false);*/

        // Start movement along the new path
        player.path.clear();
        player.path.allowDiagonal(true);
        player.path.speed(player.getSpeed())
        player.path.add(x, y, z);
        player.path.start();
    };
};

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = MovementManager; }