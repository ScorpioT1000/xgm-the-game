var PlayerManager = function() {
    var self = this;

    self.players = {};
    self.pid2cid = {};

    self.join = function(identity, credentials, clientId) {
        ige.server.log('Joining "'+identity+'"');
        if(self.findByClientId(clientId)) {
            self.removeByClientId(clientId);
        }
        self.authPlayer(identity, credentials, function(playerData) {
            self.loadPlayerCharacter(playerData, clientId);
            ige.network.send('playerJoin', { success: true, 'player': playerData }, clientId);
        }, function(reason) {
            ige.network.send('playerJoin', { success: false, message: reason }, clientId);
        });
    };

    self.leave = function(clientId) {
        var p = self.findByClientId(clientId);
        if(p) {
            ige.server.log('Leaving "'+p.identity+'"');
            self.saveByClientId(clientId);
            self.removeByClientId(clientId);
        }
    };

    self.authPlayer = function(identity, credentials, onSuccess, onFail) {
        // TODO: request database
        if((identity.indexOf('demo') === 0) && (credentials === 'demo')) {
            onSuccess && onSuccess({ id: identity, 'identity': identity });
        } else {
            onFail && onFail('Wrong identity or credentials');
        }
    };

    self.loadPlayerCharacter = function(playerData, clientId) {
        self.players[clientId] = new Unit(playerData.id)
            .addComponent(PlayerComponent)
            .streamMode(1)
            .setUpPathFinder()
            .translateToPoint(ige.server.sceneManager.getSpawnFor('player'))
            .mount(ige.server.sceneManager.getCharacterScene());
        self.players[clientId].identity = playerData.identity;
        self.pid2cid[playerData.id] = clientId;
    };

    self.saveByClientId = function(clientId) {

    };

    self.removeByClientId = function(clientId) {
        var pid = self.players[clientId].id();
        self.players[clientId].destroy();
        delete self.players[clientId];
        if(self.pid2cid[pid]) {
            delete self.pid2cid[pid];
        }
    };

    self.findByClientId = function(clientId) {
        return self.players[clientId] || null;
    };

    self.findByPlayerId = function(playerId) {
        var cid = self.pid2cid[playerId];
        return cid ? self.players[cid] : null;
    };
};

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = PlayerManager; }