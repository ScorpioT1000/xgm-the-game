var ClientNetworkEvents = new (function() {
    var self = this;

    self.host = 'http://play.indaxia.com';

    self.player = null;
    self.prepared = false;

    self._prepareCharacter = function(eid) {
        if(!self.prepared) {
            var entity = ige.$(eid);
            if(entity) {
                entity.addComponent(PlayerComponent);
                ige.client.sceneManager.getViewport().camera.trackTranslate(entity, 25);
                self.prepared = true;
            } else {
                console.log('Entity with id "'+eid+'" not found');
            }
        }
    };

    self._onMessage = function(data) {
        console.log('message', data);
    };
    
	/**
	 * Is called when a network packet with the "playerJoin" command
	 * is received by the client from the server. This is the server telling
	 * us which entity is our player entity so that we can track it with
	 * the main camera!
	 * @param data The data object that contains any data sent from the server.
	 * @private
	 */
     self._onPlayerJoin = function (data) {
        if(!data.success) {
            ige.client.log('PlayerJoin failed: '+data.message);
            return;
        }
        console.log('Login accepted', data);
        self.player = data.player;
        self._prepareCharacter(self.player.id);
	};

    self._onEntityCreated = function(entity) {
        ige.client.log('Entity created: '+entity.id());
        if (self.player && entity.id() === self.player.id) {
            self._prepareCharacter(entity.id());
        }
    };
    return self;
});

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = ClientNetworkEvents; }