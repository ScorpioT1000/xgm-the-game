var JsonMapLoader = {
    load: function(mapUrl, callback /* function(layerArray, layersById) */) {
        ige.addComponent(IgeTiledComponent);
        var jsonObj = '';
        
        if (ige.isClient) {
            var xobj = new XMLHttpRequest();
            xobj.overrideMimeType("application/json");
            xobj.open('GET', mapUrl, true);
            xobj.onload = function () {
                if (xobj.readyState == 4 && xobj.status == "200") {
                    jsonObj = JSON.parse(xobj.responseText);
                    ige.tiled.loadJson(jsonObj, callback);
                }
            };
            xobj.send(null);
        } else {
            require('fs').readFile( __dirname + '/../'+mapUrl, function (err, data) {
                if (err) {
                  throw err; 
                }
                jsonObj = JSON.parse(data.toString());
                ige.tiled.loadJson(jsonObj, callback);
            });
        }
    }
};

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = JsonMapLoader; }