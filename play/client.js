var Client = IgeClass.extend({
	classId: 'Client',
	init: function () {
		var self = this;
        
        
		// Enabled texture smoothing when scaling textures
		ige.globalSmoothing(true);

		// Enable networking
		ige.addComponent(IgeNetIoComponent);
        
		ige.addComponent(IgeEditorComponent);

		// Implement our game methods
		this.implement(ClientNetworkEvents);

		self.sceneManager = new SceneManager();

        // Create the HTML canvas
        ige.createFrontBuffer(true);
        ige.viewportDepth(true);
        
        try {

            ige.start(function (success) {
                // Check if the engine started successfully
                if (success) {
                    // Start the networking (you can do this elsewhere if it
                    // makes sense to connect to the server later on rather
                    // than before the scene etc are created... maybe you want
                    // a splash screen or a menu first? Then connect after you've
                    // got a username or something?
                    ige.network.start(self.host, function () {
                        ige.network.define('playerJoin', self._onPlayerJoin);
                        ige.network.define('message', self._onMessage);
                        
                        ige.network.addComponent(IgeStreamComponent)
                            .stream.renderLatency(16); // Render the simulation 160 milliseconds in the past
                        ige.network.stream.on('entityCreated', self._onEntityCreated);
    
                        self.sceneManager.loadMaps(function() {
                            // TODO: login form
                            ige.network.send('playerJoin', { identity: 'demo'+Math.floor(Math.random()*1000000),
                                                             credentials: 'demo' });                            
                        });
                            
                    });
                }
            });
        } catch(e) {
            alert(e.message);
        }
	}
});

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = Client; }