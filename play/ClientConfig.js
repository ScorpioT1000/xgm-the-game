var igeClientConfig = {
	include: [
		/* Your custom game JS scripts */
		'./gameClasses/ClientNetworkEvents.js',
		'./gameClasses/PlayerComponent.js',
		'./gameClasses/JsonMapLoader.js',
		'./gameClasses/SceneManager.js',

        './gameClasses/world/Unit.js',
        './gameClasses/world/Character.js',
		/* Standard game scripts */
		'./client.js',
		'./index.js'
	]
};

if (typeof(module) !== 'undefined' && typeof(module.exports) !== 'undefined') { module.exports = igeClientConfig; }